package com.example.mall.order.dao;

import com.example.mall.order.entity.OrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author
 * @email
 */
@Mapper
public interface OrderDao {

    @Select("select * from t_order where user_id=#{userId}")
    List<OrderEntity> listByUserId(Integer userId);

}
