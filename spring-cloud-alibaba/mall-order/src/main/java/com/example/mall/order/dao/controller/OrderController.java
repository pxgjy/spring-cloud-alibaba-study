package com.example.mall.order.dao.controller;


import com.example.common.utils.R;
import com.example.mall.order.entity.OrderEntity;
import com.example.mall.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author
 * @email
 * @date
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 根据用户id查询订单信息
     *
     * @param userId
     * @return
     */
    @RequestMapping("/findOrderByUserId/{userId}")
    public R findOrderByUserId(@PathVariable("userId") Integer userId) {

        //模拟异常
        if (userId == 5) {
            throw new IllegalArgumentException("非法参数异常");
        }
        //模拟超时
        if (userId == 6) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        log.info("根据userId:" + userId + "查询订单信息");
        List<OrderEntity> orderEntities = null; //= orderService.listByUserId(userId);
        return R.ok().put("orders", orderEntities);
    }

}
