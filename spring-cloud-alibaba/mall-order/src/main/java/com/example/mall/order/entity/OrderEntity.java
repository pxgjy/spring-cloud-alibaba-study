package com.example.mall.order.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author
 * @email
 */
@Data
public class OrderEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String userId;
    /**
     * 商品编号
     */
    private String commodityCode;

    private Integer count;

    private Integer amount;

}