package com.example.mall.order.service;

import com.example.mall.order.entity.OrderEntity;

import java.util.List;

/**
 * @author
 * @email
 */
public interface OrderService {


    List<OrderEntity> listByUserId(Integer userId);
}

