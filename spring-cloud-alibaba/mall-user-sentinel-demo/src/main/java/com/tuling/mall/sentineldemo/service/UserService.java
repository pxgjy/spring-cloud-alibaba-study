package com.tuling.mall.sentineldemo.service;

import com.tuling.mall.sentineldemo.entity.UserEntity;

/**
 * @Author Fox
 */
public interface UserService {

    UserEntity getById(Integer id);
}
