package com.example.mall.user.config;


import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author
 */
@Configuration
public class RestConfig {

    @Bean
    @LoadBalanced // 微服务名替换为具体的ip:port
    //@MyLoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

//    @Bean
//    public RestTemplate restTemplate(LoadBalancerInterceptor loadBalancerInterceptor) {
//        RestTemplate restTemplate = new RestTemplate();
//        //注入loadBalancerInterceptor拦截器
//        restTemplate.setInterceptors(Arrays.asList(loadBalancerInterceptor));
//        return restTemplate;
//    }


}
