package com.example.mall.user.demo;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;

import java.util.Properties;


public class NamingExample {

    public static void main(String[] args) throws NacosException {

        Properties properties = new Properties();
        properties.setProperty("serverAddr", "192.168.31.20:8848");
        //核心接口
        NamingService naming = NamingFactory.createNamingService(properties);
        //服务注册
        naming.registerInstance("mall-user", "192.168.31.21", 8889, "ppppp");
        //服务发现
        System.out.println(naming.getAllInstances("mall-user"));


    }
}